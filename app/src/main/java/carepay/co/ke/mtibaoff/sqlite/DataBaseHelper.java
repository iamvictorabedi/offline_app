package carepay.co.ke.mtibaoff.sqlite;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "CarepayOff";

    //database version
    private static final int DB_VERSION = 1;

    //Constructor
    public DataBaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        // create notes table
        db.execSQL(PriceList.CREATE_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + PriceList.TABLE_NAME);

        // Create tables again
        onCreate(db);
    }

//    public long insertNote(String note) {
//        // get writable database as we want to write data
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        ContentValues values = new ContentValues();
//        // `id` and `timestamp` will be inserted automatically.
//        // no need to add them
//        values.put(Note.COLUMN_NOTE, note);
//
//        // insert row
//        long id = db.insert(Note.TABLE_NAME, null, values);
//
//        // close db connection
//        db.close();
//
//        // return newly inserted row id
//        return id;
//    }

    public PriceList getSingleItem(long id) {
        // get readable database as we are not inserting anything
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(PriceList.TABLE_NAME,
                new String[]{PriceList.COLUMN_ID, PriceList.COLUMN_NAME, PriceList.COLUMN_AMOUNT, PriceList.COLUMN_CODE},
                PriceList.COLUMN_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();

        // prepare note object
        PriceList priceList = new PriceList(
                cursor.getInt(cursor.getColumnIndex(PriceList.COLUMN_ID)),
                cursor.getString(cursor.getColumnIndex(PriceList.COLUMN_NAME)),
                cursor.getString(cursor.getColumnIndex(PriceList.COLUMN_CODE)),
                cursor.getDouble(cursor.getColumnIndex(PriceList.COLUMN_AMOUNT)));


                // close the db connection
        cursor.close();

        return priceList;
    }

    public List<PriceList> getAllItems() {
        List<PriceList> priceLists = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + PriceList.TABLE_NAME + " ORDER BY " +
                PriceList.COLUMN_ID + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                PriceList priceList= new PriceList();
                priceList.setProduct_id(cursor.getInt(cursor.getColumnIndex(PriceList.COLUMN_ID)));
                priceList.setProduct_name(cursor.getString(cursor.getColumnIndex(PriceList.COLUMN_NAME)));
                priceList.setProduct_amount(Double.valueOf(cursor.getString(cursor.getColumnIndex(PriceList.COLUMN_AMOUNT))));
                priceList.setProduct_code(cursor.getString(cursor.getColumnIndex(PriceList.COLUMN_CODE)));
                priceLists.add(priceList);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return notes list
        return priceLists;
    }

//    public int getNotesCount() {
//        String countQuery = "SELECT  * FROM " + ContactsContract.CommonDataKinds.PriceList.TABLE_NAME;
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor cursor = db.rawQuery(countQuery, null);
//
//        int count = cursor.getCount();
//        cursor.close();
//
//
//        // return count
//        return count;
//    }

//public List<PriceList> updatePriceList(PriceList priceList) {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        ContentValues values = new ContentValues();
//        values.putAll(PriceList.COLUMN_ID, priceList.getProduct_id(), PriceList.COLUMN_NAME, priceList.getProduct_name(),
//                PriceList.COLUMN_CODE, priceList.getProduct_code(), PriceList.COLUMN_AMOUNT, priceList.getProduct_amount());
//
//        // updating row
//        return db.update(PriceList.TABLE_NAME, values, ContactsContract.CommonDataKinds.Note.COLUMN_ID + " = ?",
//                new String[]{String.valueOf(note.getId())});
//    }

}