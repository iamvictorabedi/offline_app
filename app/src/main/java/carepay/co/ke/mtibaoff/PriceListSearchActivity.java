package carepay.co.ke.mtibaoff;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

public class PriceListSearchActivity extends AppCompatActivity {
    MaterialSearchView searchView;
    ListView lstView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_price_list_search);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Search Price List Items");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setTitleTextColor(Color.parseColor("#FFFFFF"));
            readPriceListData();

        lstView = (ListView)findViewById(R.id.lstView);
        ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,pricelist_data);
        lstView.setAdapter(adapter);


        searchView = (MaterialSearchView)findViewById(R.id.search_view);
        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {

            }

            @Override
            public void onSearchViewClosed() {

                //If closed Search View , lstView will return default
                lstView = (ListView)findViewById(R.id.lstView);
                ArrayAdapter adapter = new ArrayAdapter(PriceListSearchActivity.this,android.R.layout.simple_list_item_1,pricelist_data);
                lstView.setAdapter(adapter);

            }
        });

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(newText != null && !newText.isEmpty()){
                    List<PriceList> pricelist_data = new ArrayList<>();
                    for(PriceList item:pricelist_data){
                        if(item.equals(newText))
                            pricelist_data.add(item);
//                        if(item.contains(newText))
//                            lstFound.add(item);
                    }
//                    }

                    ArrayAdapter adapter = new ArrayAdapter(PriceListSearchActivity.this,android.R.layout.simple_list_item_1,pricelist_data);
                    lstView.setAdapter(adapter);
                }
                else{
                    //if search text is null
                    //return default
                    ArrayAdapter adapter = new ArrayAdapter(PriceListSearchActivity.this,android.R.layout.simple_list_item_1,pricelist_data);
                    lstView.setAdapter(adapter);
                }
                return true;
            }

        });




    }
    private List<PriceList> pricelist_data = new ArrayList<>();
    private void readPriceListData()  {
      InputStream incomming_data = getResources().openRawResource(R.raw.price_list);
      BufferedReader restructuredata = new BufferedReader(
              new InputStreamReader(incomming_data, Charset.forName("UTF-8"))
      );
      String line = "";
      try {
          restructuredata.read();
          while ( (line = restructuredata.readLine()) !=null){
              Log.d("PriceListActivity","Just Proceeds" + line);
              //SPlit line with ","
              String[] price = line.split(",");
              //Read and map data to PriceList List Above
              PriceList priceList = new PriceList();
              priceList.setProduct_name(price[2]);
              priceList.setProduct_code(price[1]);
              priceList.setPrice_amount(price[3]);
              //Finally added csv items to my array list
              pricelist_data.add(priceList);
              Log.d("PriceListActivity","Just Proceeds" + priceList);
          }
      }catch (Exception e){
          Log.wtf("MyActivity","Error in reading pricelist file"+ line,e);
          e.printStackTrace();
      }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_item,menu);
        MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);
        return true;
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
