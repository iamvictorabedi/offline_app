package carepay.co.ke.mtibaoff.sqlite;

import android.database.sqlite.SQLiteDatabase;

public class PriceList {
    //Constants for Database name, table name, and column names
    public static final String TABLE_NAME = "pricelist";
    public static final String COLUMN_ID = "product_id";
    public static final String COLUMN_NAME = "product_name";
    public static final String COLUMN_AMOUNT = "product_amount";
    public static final String COLUMN_CODE = "product_code";

    //CREATING  TABLE
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME
                + "(" + COLUMN_ID +
                " INTEGER PRIMARY KEY, " + COLUMN_NAME +
                " VARCHAR, " + COLUMN_CODE +
                " VARCHAR," + COLUMN_AMOUNT + " BIGINT);";


    private int product_id;
    private String product_name;
    private String product_code;
    private Double product_amount;

    public PriceList() {

    }

    public PriceList(int product_id, String product_name, String product_code, Double product_amount) {
        this.product_id = product_id;
        this.product_name = product_name;
        this.product_code = product_code;
        this.product_amount = product_amount;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_code() {
        return product_code;
    }

    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }

    public Double getProduct_amount() {
        return product_amount;
    }

    public void setProduct_amount(Double product_amount) {
        this.product_amount = product_amount;
    }
}
