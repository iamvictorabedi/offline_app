package carepay.co.ke.mtibaoff;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;


public class BillingForm extends AppCompatActivity {
EditText pricelist,diagnosis;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_billing_form);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        diagnosis = findViewById(R.id.selectedDiagnosis);
        pricelist = findViewById(R.id.selectedItem);

        diagnosis.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Intent diagnosis = new Intent(getApplicationContext(),DiagnosiseSearchActivity.class);
                startActivity(diagnosis);
                return false;
            }
        });
        pricelist.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Intent pricelist = new Intent(getApplicationContext(),PriceListSearchActivity.class);
                startActivity(pricelist);
                return false;
            }
        });
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
