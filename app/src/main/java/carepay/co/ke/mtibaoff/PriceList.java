package carepay.co.ke.mtibaoff;

public class PriceList {

    private String product_code;
    private String product_name;
    private String price_amount;


    public String getProduct_code() {
        return product_code;
    }

    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getPrice_amount() {
        return price_amount;
    }

    public void setPrice_amount(String price_amount) {
        this.price_amount = price_amount;
    }

    @Override
    public String toString() {
        return "PriceList{" +
                "product_code='" + product_code + '\'' +
                ", product_name='" + product_name + '\'' +
                ", price_amount=" + price_amount +
                '}';
    }
}
