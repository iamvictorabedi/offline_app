package carepay.co.ke.mtibaoff;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SmsReaderActivity extends AppCompatActivity {
Button startbill;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        startbill = findViewById(R.id.start_bill);
        startbill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent billform = new Intent(getApplicationContext(), BillingForm.class);
                billform.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(billform);
            }
        });
    }
}
